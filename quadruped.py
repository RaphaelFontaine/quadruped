import argparse
import math
import numpy as np
import pybullet as p
from time import sleep
import matplotlib.pyplot as pyplot

dt = 0.01
dz = 0.02
l0 = 0.04
l1 = 0.045
l2 = 0.065
l3 = 0.087

def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot

    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])



"*************************************** Fonction distance *********************************************"


def distance(a,b):
    """Renvoie la distance entre 2 points a et b dans un repère en 3 dimensions

    Arguments:
        a, b : tableaux de flottants (3 cases par tableaux)

    Returns:
        distance : flottant
    """
    long1 = (a[0]-b[0])**2
    long2 = (a[1]-b[1])**2
    long3 = (a[2]-b[2])**2

    distance = (long1 + long2 + long3)**0.5
    return distance


"*************************************** Fonction interpolation ****************************************"


def interpolation(xs,ts,t):
    """
    Arguments : xs -> tableau des valeurs de x en fonction de t (dans ts)
                ts -> tableau des valeurs de t

    Returns : x correspondant à t

    """
    x = 0
    if (t<=ts[0]):
        x = xs[0]
    elif (t>=ts[len(ts)-1]):
        x = xs[len(xs)-1]
    else:
        k = 0
        while (ts[k]<=t):
            k = k + 1
        coef = (xs[k] - xs[k-1]) / (ts[k]-ts[k-1])
        x = xs[k-1] + coef * (t - ts[k-1])
    return x

print("------Test sur Interpolation()--------")
xs = [0,2,4,9,0]
ts = [0,3,6,9,12]
print(interpolation(xs,ts,10))
print("OK \n")


def interpolation3(xs,ys,zs,ts,t):
    """
    Arguments : xs, ys, zs -> tableau des valeurs de x, y, z en fonction de t (dans ts)
                ts -> tableau des valeurs de t

    Returns : (x,y,z) correpsondant à t


    """
    x = interpolation(xs,ts,t)
    y = interpolation(ys,ts,t)
    z = interpolation(zs,ts,t)
    return (x,y,z)

print("-------Test sur Interpolation3()------")
xs = [0,2,4,9,0]
ys = [3,5,1,7,12]
zs = [0,2,4,6,8]
ts = [0,3,6,9,12]
print(interpolation3(xs,ys,zs,ts,4))
print("OK \n")


def interpolation_joints(J0, J1, T, t):
    """
    Arguments : J0 ->tableau d'angles (comme joints 12 valeurs)
                J1 ->tableau d'angles (comme joints 12 valeurs)
                T  ->tableau des valeurs de t

    Returns : un tableau joints correpondant aux angles au moment t

    """
    joints = [0]*12
    for k in range (len(J0)) :
        joints[k] = interpolation([J0[k],J1[k]], T, t)
    return joints




"*************************************** Fonction position_depart ****************************************"

def position_depart(t):
    """
    Donne une position stable au robot, il le met en position araigné
    Utilise l'interpolation, il met le robot dans cette position pour 0.05<=t<=0.45

    """
    J0 = [0]*12
    joints = [0]*12
    joints[2] = math.pi/2
    joints[5] = math.pi/2
    joints[8] = math.pi/2
    joints[11] = math.pi/2
    T = [0.05, 0.45]
    return interpolation_joints(J0, joints, T, t)




"*************************************** MODE DEMO ****************************************"


def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs (joints[])
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints




"*************************************** MODE LEG_IK ****************************************"


def set_angles_to_leg(leg_id, alpha1, alpha2, alpha3, joints):
    """

    Arguments : leg_id -> numero de la patte
                alpha1, alpha2, alpha3 -> trois angles
                joints ->tableau d'angles (12 cases)

    Return : joints en ayant modifié les 3 cases correspondant à la patte leg_id par les 3 angles

    """
    joints[leg_id*3:leg_id*3+3] = (alpha1, alpha2, alpha3)
    return joints


def leg_ik(x,y,z):
    """controler la position du bout d'une seule patte

    Arguments:
        (x,y,z) : tableau de coordonnées du bout de la patte dans le repère

    Returns:
        (alpha1,alpha2,aplha3) : tableau des angles (en radian) des 3 parties de la patte
    """

    alpha1 = math.atan2(y,x)
    A = [l1*math.cos(alpha1), l1*math.sin(alpha1), 0]
    M = [x,y,z]
    d = distance(A,M)
    # Détermination alpha2
    #condition qui nous permet de savoir si une position est possible ou non
    #car d correspond à la longueur entre le point (auquel on vet accéder) et le bout
    #du morceau de la patte de longueur l1
    #la distance maximale atteignable est donc l2 + l3
    if ( (l3-l2) <= d <= (l2+l3) ) :
        a = ( l2**2 - l3**2 + d**2 ) / (2*l2*d)
        beta1 = math.acos(a)
        beta2 = math.asin(z/d)
        alpha2 = beta1 + beta2
        # Théorème d'Al Kashi
        alpha3 = math.pi - math.acos( ( l2**2 + l3**2 - d**2 ) / (2*l2*l3))

        # Affectation de la valeur des angles obtenue par le modèle de la cinématique inverse
        tab = [alpha1, alpha2, alpha3]

        return tab

"*************************************** MODE ROBOT_IK ****************************************"

def get_default_pos():
    """

    Returns : les coordonnées x, y, z du bout des pattes lorsque le robot est dans sa position stable (postion_depart)

    """
    x = l1+l2
    y = 0
    z = -l3
    return x,y,z

print("-----------------Test get_default_pos()----------------")
print(get_default_pos())
print("OK\n")


def get_angle(legID):
    """

    Arguments : legID -> numero de la patte

    Returns : l'angle theta de la patte legID (par convention)
    """
    theta = 0
    if (legID == 0):
        theta = 3 * math.pi/4
    elif (legID == 1):
        theta = -3 * math.pi/4
    elif (legID == 2):
        theta = -math.pi/4
    elif (legID == 3):
        theta = math.pi/4
    return theta

print("-----------------Test get_angle()----------------")
print("Pour patte = 1 --> ", get_angle(1))
print("OK\n")
print("Pour patte = 3 --> ", get_angle(3))
print("OK\n")


def rotation_x(theta,x, y):
    rot = x * math.cos(theta) + y * math.sin(theta)
    return rot

def rotation_y(theta,x, y):
    rot = y * math.cos(theta) - x * math.sin(theta)
    return rot


def ComputeIKLeg(legID, x, y, z, extra_theta=0):
    """
    Arguments : legID -> numero de la patte
                x y z -> coordonnés du centre du robot auxquels on souhaite accéder

    Returns : un tableau avec les 3 angles de la patte legID tel que le centre du robot soit au
              coordonnés x y z

    """


    default_x, default_y, default_z = get_default_pos()
    theta = get_angle(legID) + extra_theta
    new_x = default_x - rotation_x(theta, x, y)
    new_y = default_y - rotation_y(theta, x, y)
    new_z = default_z - z
    tab = leg_ik(new_x, new_y, new_z)
    return tab


def robot_ik(x,y,z):
        """
        Arguments : x, y, z ->nouvelle position du centre du robot (les coordonnées (0,0,O) correspondent aux
        coordonnées lorsque le robot est dans sa postion stable de départ)

        Returns : un tableau "robot" contenant les 12 angles du robot (comme joints)

        """
        patte1 = ComputeIKLeg(0, x, y, z,extra_theta=0)
        patte2 = ComputeIKLeg(1, x, y, z,extra_theta=0)
        patte3 = ComputeIKLeg(2, x, y, z,extra_theta=0)
        patte4 = ComputeIKLeg(3, x, y, z,extra_theta=0)
        robot = patte1 + patte2 + patte3 + patte4
        return robot


"*************************************** MODE WALK ****************************************"


"************************ ROTATION **************************"


def move_circ1(t, joints, theta, mod):
    """
    Arguments : joints -> tableau des angles
                theta ->angle de rotation
                mod -> modulo (permet de déterminer quand on va faire la rotation pour la patte1)

    Returns : le tableau des angles joints modifié de telle sorte que la patte 1 a fait une rotation de theta
    """
    x0, y0, z0 = get_default_pos()
    xs = [x0, x0-x0*math.cos(theta)/2, x0*math.cos(theta), x0*math.cos(theta)]
    ys = [y0, x0*math.sin(theta)/2, x0*math.sin(theta), x0*math.sin(theta)]
    zs = [z0, z0+dz/2, z0, z0]
    ts = [0, mod/4, mod/2-mod/10, mod/2]
    tab = [0]*3
    tab = interpolation3(xs, ys, zs, ts, t)
    coor = leg_ik(tab[0], tab[1], tab[2])
    joints[0] = coor[0]
    joints[1] = coor[1]
    joints[2] = coor[2]
    return joints


def move_circ2(t, joints, theta, mod):
    """

    Returns : le tableau des angles joints modifié de telle sorte que la patte 2 a fait une rotation de theta
    """
    x0, y0, z0 = get_default_pos()
    xs = [x0, x0-x0*math.cos(theta)/2, x0*math.cos(theta), x0*math.cos(theta)]
    ys = [y0, x0*math.sin(theta)/2, x0*math.sin(theta), x0*math.sin(theta)]
    zs = [z0, z0+dz/2, z0, z0]
    ts = [mod/2, 3*mod/4, mod-mod/10, mod]
    tab = [0]*3
    tab = interpolation3(xs, ys, zs, ts, t)
    coor = leg_ik(tab[0], tab[1], tab[2])
    joints[3] = coor[0]
    joints[4] = coor[1]
    joints[5] = coor[2]
    return joints


def move_circ3(t, joints, theta, mod):
    """

    Returns : le tableau des angles joints modifié de telle sorte que la patte 3 a fait une rotation de theta
    """
    x0, y0, z0 = get_default_pos()
    xs = [x0, x0-x0*math.cos(theta)/2, x0*math.cos(theta), x0*math.cos(theta)]
    ys = [y0, x0*math.sin(theta)/2, x0*math.sin(theta), x0*math.sin(theta)]
    zs = [z0, z0+dz/2, z0, z0]
    ts = [0, mod/4, mod/2-mod/10, mod/2]
    tab = [0]*3
    tab = interpolation3(xs, ys, zs, ts, t)
    coor = leg_ik(tab[0], tab[1], tab[2])
    joints[6] = coor[0]
    joints[7] = coor[1]
    joints[8] = coor[2]
    return joints


def move_circ4(t, joints, theta, mod):
    """

    Returns : le tableau des angles joints modifié de telle sorte que la patte 4 a fait une rotation de theta
    """
    x0, y0, z0 = get_default_pos()
    xs = [x0, x0-x0*math.cos(theta)/2, x0*math.cos(theta), x0*math.cos(theta)]
    ys = [y0, x0*math.sin(theta)/2, x0*math.sin(theta), x0*math.sin(theta)]
    zs = [z0, z0+dz/2, z0, z0]
    ts = [mod/2, 3*mod/4, mod-mod/10, mod]
    tab = [0]*3
    tab = interpolation3(xs, ys, zs, ts, t)
    coor = leg_ik(tab[0], tab[1], tab[2])
    joints[9] = coor[0]
    joints[10] = coor[1]
    joints[11] = coor[2]
    return joints


def mv_circulaire(t, joints, t_speed):
    """
    Arguments : joints -> tableau des angles
                t_speed -> vitesse de rotation

    Returns : le tableau des angles joints tel que toutes les pattes ont une rotation de vitesse t_speed
    """
    if (t_speed == 0):
        return joints
    if (t_speed < 0):
        theta = -math.pi/4 - math.asin(l0 * math.sin(math.pi/4)/(l1+l2))
    if (t_speed > 0):
        theta = math.pi/4 + math.asin(l0 * math.sin(math.pi/4)/(l1+l2))
    mod = abs(math.pi/4/t_speed)
    t = t%mod
    joints = move_circ1(t, joints, theta, mod)
    joints = move_circ2(t, joints, theta, mod)
    joints = move_circ3(t, joints, theta, mod)
    joints = move_circ4(t, joints, theta, mod)
    return joints




"********************* DEPLACEMENT *************************"

def cinematique_directe(a, b, c):
    """
    Returns : les coordonnés x y z (dans le repère de la patte) sachant que la patte a les angles a b et c

    """

    x = math.cos(a) * (l1 + l2*math.cos(b) + l3*math.cos(c-b))
    y = math.sin(a) * (l1 + l2*math.cos(b) + l3*math.cos(b-c))
    z = l2*math.sin(b) + l3*math.sin(b-c)
    return x, y, z

a = math.pi/4
b = -math.pi/2
c = -math.pi/2
print("Test cin directe", cinematique_directe(a,b,c))



def deplacement1(t, joints, mod, p, tour):
    """
    Arguments : joints -> tableau des angles
                mod -> modulo (permet de déterminer quand on va bouger la patte1)
                p -> angle de la patte 1 du robot une fois qu'on a bougé son centre avec ROBOT_IK
                tour -> tour = 1 on bouge la patte 2 et 4 en premier
                        tour = 2 on bouge la patte 1 et 3 en premier
                        on fait ca pour éviter une déviation du robot

    Return : joints modifié de telle sorte que la patte 1 soit de nouveau dans sa position stable
    """
    arr = get_default_pos()
    x0 = p[0]
    y0 = p[1]
    z0 = p[2]
    x1 = arr[0]
    y1 = arr[1]
    z1 = arr[2]
    xs = [x0, (x0+x1)/2, x1]
    ys = [y0, (y0+y1)/2, y1]
    zs = [z0, z0+dz, z1]
    if (tour == 1):
        ts = [3*mod/5, 4*mod/5, 4.999*mod/5]
    elif (tour == 2):
        ts = [mod/5, 2*mod/5, 3*mod/5]
    pos = interpolation3(xs, ys, zs, ts, t)
    tab = leg_ik(pos[0], pos[1], pos[2])
    joints[0] = tab[0]
    joints[1] = tab[1]
    joints[2] = tab[2]
    return joints

def deplacement2(t, joints, mod, p, tour):
    """
    Return : joints modifié de telle sorte que la patte 2 soit de nouveau dans sa position stable
    """
    arr = get_default_pos()
    x0 = p[0]
    y0 = p[1]
    z0 = p[2]
    x1 = arr[0]
    y1 = arr[1]
    z1 = arr[2]
    xs = [x0, (x0+x1)/2, x1]
    ys = [y0, (y0+y1)/2, y1]
    zs = [z0, z0+dz, z1]
    if (tour == 2):
        ts = [3*mod/5, 4*mod/5, 4.999*mod/5]
    elif (tour == 1):
        ts = [mod/5, 2*mod/5, 3*mod/5]
    pos = interpolation3(xs, ys, zs, ts, t)
    tab = leg_ik(pos[0], pos[1], pos[2])
    joints[3] = tab[0]
    joints[4] = tab[1]
    joints[5] = tab[2]
    return joints

def deplacement3(t, joints, mod, p, tour):
    """
    Return : joints modifié de telle sorte que la patte 3 soit de nouveau dans sa position stable
    """
    arr = get_default_pos()
    x0 = p[0]
    y0 = p[1]
    z0 = p[2]
    x1 = arr[0]
    y1 = arr[1]
    z1 = arr[2]
    xs = [x0, (x0+x1)/2, x1]
    ys = [y0, (y0+y1)/2, y1]
    zs = [z0, z0+dz, z1]
    if (tour == 1):
        ts = [3*mod/5, 4*mod/5, 4.999*mod/5]
    elif (tour == 2):
        ts = [mod/5, 2*mod/5, 3*mod/5]
    pos = interpolation3(xs, ys, zs, ts, t)
    tab = leg_ik(pos[0], pos[1], pos[2])
    joints[6] = tab[0]
    joints[7] = tab[1]
    joints[8] = tab[2]
    return joints

def deplacement4(t, joints, mod, p, tour):
    """
    Return : joints modifié de telle sorte que la patte 4 soit de nouveau dans sa position stable
    """
    arr = get_default_pos()
    x0 = p[0]
    y0 = p[1]
    z0 = p[2]
    x1 = arr[0]
    y1 = arr[1]
    z1 = arr[2]
    xs = [x0, (x0+x1)/2, x1]
    ys = [y0, (y0+y1)/2, y1]
    zs = [z0, z0+dz, z1]
    if (tour == 2):
        ts = [3*mod/5, 4*mod/5, 4.999*mod/5]
    elif (tour == 1):
        ts = [mod/5, 2*mod/5, 3*mod/5]
    pos = interpolation3(xs, ys, zs, ts, t)
    tab = leg_ik(pos[0], pos[1], pos[2])
    joints[9] = tab[0]
    joints[10] = tab[1]
    joints[11] = tab[2]
    return joints


def deplacement_centre_x(t, joints, mod):
    """
    On déplace le centre du robot de 0.07 vers les x en utilisant l'interpolation
    Returns : joints
    """
    xs = [0, 0.07]
    ts = [0, 1.6*mod/10]
    x = interpolation(xs, ts, t)
    joints = robot_ik(x,0,0)
    return joints

def deplacement_centre_y(t, joints, mod):
    """
    On déplace le centre du robot de 0.07 vers les y en utilisant l'interpolation
    Returns : joints
    """
    ys = [0, 0.07]
    ts = [0, mod/10]
    y = interpolation(ys, ts, t)
    joints = robot_ik(0,y,0)
    return joints

def coord_mv_centre(joints, x, y, z, leg):
    """
    Returns : les coordonnées du bout de la patte leg (dans son repère) une fois qu'on a déplacé son centre (avec ROBOT_IK)
    """
    joints = robot_ik(x, y, z)
    if (leg ==1):
        p = cinematique_directe(joints[0], joints[1], joints[2])
    elif (leg ==2):
        p = cinematique_directe(joints[3], joints[4], joints[5])
    elif (leg ==3):
        p = cinematique_directe(joints[6], joints[7], joints[8])
    elif (leg ==4):
        p = cinematique_directe(joints[9], joints[10], joints[11])
    return p


def deplacement_x(t, joints, t_speed):
    """
    Arguments : joints -> tableau des angles
                t_speed -> vitesse de déplacement vers les x

    Returns : joints en sachant que le robot se déplace à la vitesse t_speed vers les x

    """
    mod = 0.07/t_speed
    p1 = coord_mv_centre(joints, 0.07, 0, 0, 1)
    p2 = coord_mv_centre(joints, 0.07, 0, 0, 2)
    p3 = coord_mv_centre(joints, 0.07, 0, 0, 3)
    p4 = coord_mv_centre(joints, 0.07, 0, 0, 4)
    if (0 <= t%mod < mod/5):
        joints = deplacement_centre_x(t%mod, joints, mod)
        return joints
    elif (mod/5<= t%mod <mod):
        if ((t % 2*mod) < mod):
            tour=1
        else:
            tour=2

        joints = deplacement2(t%mod, joints, mod, p2, tour)
        joints = deplacement4(t%mod, joints, mod, p4, tour)
        joints = deplacement1(t%mod, joints, mod, p1, tour)
        joints = deplacement3(t%mod, joints, mod, p3, tour)
        return joints

def deplacement_y(t, joints, t_speed):
    """
    Arguments : joints -> tableau des angles
                t_speed -> vitesse de déplacement vers les y

    Returns : joints en sachant que le robot se déplace à la vitesse t_speed vers les y

    """
    mod = 0.07/t_speed
    p1 = coord_mv_centre(joints, 0, 0.07, 0, 1)
    p2 = coord_mv_centre(joints, 0, 0.07, 0, 2)
    p3 = coord_mv_centre(joints, 0, 0.07, 0, 3)
    p4 = coord_mv_centre(joints, 0, 0.07, 0, 4)
    if (0 <= t%mod < mod/5):
        joints = deplacement_centre_y(t%mod, joints, mod)
        return joints
    elif (mod/5<= t%mod <mod):
        if ((t % 2*mod) < mod):
            tour=2
        else:
            tour=1

        joints = deplacement2(t%mod, joints, mod, p2, tour)
        joints = deplacement4(t%mod, joints, mod, p4, tour)
        joints = deplacement1(t%mod, joints, mod, p1, tour)
        joints = deplacement3(t%mod, joints, mod, p3, tour)
        return joints


def deplacement(t, joints, x_speed , y_speed, t_speed):
    """
    Arguments : joints -> tableau des angles
                t_speed -> vitesse de rotation
                x_speed -> vitesse de déplacement vers les x
                y_speed -> vitesse de déplacement vers les y

    Returns : joints en sachant que le robot se déplace à la vitesse x_speed vers les x, y_speed vers les y,
              et avec une vitesse de rotation t_speed

    """
    k=0
    print(t)
    if (x_speed!=0):
        mod1 = 0.07/(x_speed*3)
    else:
        mod1=0
        k=k+1
    if (y_speed!=0):
        mod2 = 0.07/(y_speed*3)
    else:
        mod2=0
        k=k+1
    if (t_speed!=0):
        mod3 = abs(math.pi/4/(t_speed*3))
    else:
        mod3=0
        k=k+1
    if (k==3):
        return joints
    if (t%(mod1+mod2+mod3) < mod1-mod1/10):
        joints = deplacement_x(t, joints, 3*x_speed)
    elif (mod1+mod1/10 <=t%(mod1+mod2+mod3) < mod1+mod2-mod2/10):
        joints = deplacement_y(t, joints, 3*y_speed)
    elif (mod1+mod2+mod2/10 <= t%(mod1+mod2+mod3) < mod1+mod2+mod3-mod3/10):
        joints = mv_circulaire(t, joints, 3*t_speed)
    return joints

"***************************************** GOTO ******************************************"




"***************************************** MAIN ******************************************"


if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t = args.m, args.x, args.y, args.t

    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')

    elif mode == 'leg_ik':
        x = p.addUserDebugParameter("x", 0, l1+l2+l3, l1+l2)
        y = p.addUserDebugParameter("y", -0.10, 0.10, 0)
        z = p.addUserDebugParameter("z", -0.075, 0.13, -0.075)
        print('Mode leg_ik...')

    elif mode == 'robot_ik':
        x = p.addUserDebugParameter("x", -0.05, 0.05, 0.0)
        y = p.addUserDebugParameter("y", -0.05, 0.05, 0.0)
        z = p.addUserDebugParameter("z", -0.05, 0.05, 0.0)
        print('Mode robot_ik...')

    elif mode == 'walk':
        x_speed = p.addUserDebugParameter("x_speed", -0.1, 0.1, 0.07/3)
        y_speed = p.addUserDebugParameter("y_speed", -0.1, 0.1, 0.07/3)
        t_speed = p.addUserDebugParameter("t_speed", -math.pi/3, math.pi/3, math.pi/12)
        print('Mode walk...')

    else:
        raise Exception('Mode non implémenté: %s' % mode)

    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            joints = demo(t, p.readUserDebugParameter(amplitude))


        elif mode == 'leg_ik':
            # Récupération des positions cibles
            if (0<=t<0.5):
                joints = position_depart(t)
            else:
                tab = leg_ik(p.readUserDebugParameter(x), p.readUserDebugParameter(y), p.readUserDebugParameter(z))
                patte = 3
                joints = set_angles_to_leg(patte, tab[0], tab[1], tab[2], joints)
        # Envoi des positions cibles au simulateur

        elif mode == 'robot_ik':
            if (0<=t<0.5):
                joints = position_depart(t)
            else:
                joints = robot_ik(p.readUserDebugParameter(x), p.readUserDebugParameter(y), p.readUserDebugParameter(z))


        elif mode == 'walk':
            if (0<=t<0.5):
                joints = position_depart(t)
            else:
                #Tester uniquement avec une vitesse de rotation constante pi/4 rad/s
                #joints = mv_circulaire(t-0.5, joints, math.pi/4)
                #Tester avec une vitesse de déplacement vers les x constante 0.07 m/s
                #joints = deplacement_x(t-0.5, joints, 0.07)
                #Tester avec une vitesse de déplacement vers les y constante 0.07 m/s
                #joints = deplacement_y(t-0.5, joints, 0.07)
                #Tester le mode walk
                joints = deplacement(t-0.5, joints, p.readUserDebugParameter(x_speed), p.readUserDebugParameter(y_speed), p.readUserDebugParameter(t_speed))

        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
