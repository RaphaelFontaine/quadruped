Nom : Fontaine Raphael

Explication:
  LEG_IK : le mode leg_ik fonctionne correctement, le seul problème est quand on prend x=y=0. En effet, je n'ai pas
           su gérer ce cas

  ROBOT_IK : le mode robot_ik fonctionne correctement

  WALK : J'ai décomposer le mode walk en 3 parties : mv_circulaire, deplacement_x, deplacement_y

      ->mv_circulaire : faire une rotation theta au robot. Pour cela, on fait faire la rotation d'abord aux pattes 1 et 3 puis
                        2 et 4. Cette fonction marche correctement.
                        L'idée pour faire une rotation de theta (que l'utilisateur choisit avec le curseur)a été de fixer la rotation des pattes à pi/4. Puis on calcule le modulo mod = abs(math.pi/4/t_speed). On fait alors la rotation de pi/4 pendant une durée mod. De cette manière la vitesse de rotation est bien t_speed

      ->deplacement_x : On a un peu repris la même idée pour cette fonction. On a fixé le déplacement à 0.07 avec ROBOT_IK.
                        Puis on calcule le modulo de manière à ce que le robot fasse bien un déplacement avec une vitesse x_speed
                        Pour faire bouger le robot, on bouge (au tour1) les pattes 2 et 4 en même temps puis les pattes 1 et 3,  puis (au tour 2) on inverse les rôles afin que la déplacement soit bien droit. En effet si on n'inverse pas une fois sur deux, le robot va devier car lorsque le corps bascule vers l'avant et qu'on bouge ensuite les deux pattes(une à l'avant, une à l'arrière), la patte avant va frotter le sol car le corps du robot est vers l'avant. La patte arrière va se lever correctement. Pendant la 2ème phase (les 2 dernières pattes), les deux pattes se lèvent correctement et aucune n'est en contact avec le sol donc il n'y a pas de frottements. Si on resume, il y a des frottements pour la 1er phase et pas pour la 2ème, c'est pourquoi j'inverse l'ordre des pattes à chaque tour.
                        Cette fonction ne marche pas parfaitement. Pour une vitesse de 0.07 m/s, cela marche correctement. Mais pour les autres vitesses, cela ne marche pas parfaitment, il y a des mouvements brusques parfois, mais je n'arrive pas à l'expliquer. En effet le mouvement est censé être le même (avancement de 0.07 m), seul le temps pour y parvenir doit changer, c'est pourquoi je ne comprend pas les erreurs

      ->deplacement_y : même idée que deplacement_x à la différence que l'on avance le corps du robot du 0.07 m vers les y.


      -> walk : Pour cette fonction, je fais d'abord le deplacement suivant les x, puis suivant les y, puis la rotation


  GOTO : Je n'ai pas pu implementer ce mode, mais si j'avais du le faire, j'aurai fait faire au robot une rotation (mv_circulaire) de manière à ce qu'il n'ait plus qu'à avancer tout droit (deplacement_x) pour se rendre au point (x,y). 
